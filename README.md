# dcmetadata-docs

[![Documentation Status](https://readthedocs.org/projects/data-central/badge/?version=latest)](https://data-central.readthedocs.io/en/latest/?badge=latest)


Documentation on the required file structure and metadata to ingest data into Data Central. 

## Deploy



### Dev

This repo extends the docker-sphinx-doc image (https://github.com/keimlink/docker-sphinx-doc) which utilizes 
sphinx-autobuild (https://github.com/GaretJax/sphinx-autobuild) to rebuild documentation when a change is detected 
and includes a live reload web server.  

Set up .env
```console
    ENV=dev
    HOST_PORT=8003
```

Build the image
```console
    docker-compose -f docker-compose.dev.yml build --no-cache
```

Start Container(s)
```console
    docker-compose -f docker-compose.dev.yml up -d
    docker-compose -f docker-compose.dev.yml logs web
```
    
The documentation will be served at http://127.0.0.1:8003/ and will rebuild when a file is changed. 


### Prod

Set up .env
```console
    ENV=prod
    HOST_PORT=8003
```

Build the image
```console
    docker-compose -f docker-compose.prod.yml build --no-cache
```

Start Container(s)
```console
    docker-compose -f docker-compose.prod.yml up -d
    docker-compose -f docker-compose.prod.yml logs web
```

(Re)start Container(s)
```console
    sudo docker-compose -f docker-compose.prod.yml restart web
```

Note - we use Read The Docs for automatic build and hosting: https://data-central.readthedocs.io/en/latest/ 
(web hook configured as per https://docs.readthedocs.io/en/stable/webhooks.html)

### Useful

Run `make html` to build dcmetadata documentation locally.

Useful site for converting html tables to rst: https://truben.no/table/

Ref for rst: http://docutils.sourceforge.net/docs/user/rst/quickref.html#field-lists