Processes
=========

If you have any questions about this documentation during the preparation of your data release, please `contact us <https://datacentral.org.au/contact/>`_.

Response Timescales
-------------------
We endeavour to implement any change-requests as soon as possible, however the DC team is currently relatively small.
Please see below for a rough estimate on update timescales. Please note, these are a guide
only, subject to the complexity of the update requested, and additional constraints on our time
(e.g., other survey-team data releases, conferences, funding milestones etc).

Data and Schema Changes
^^^^^^^^^^^^^^^^^^^^^^^
For critical changes (e.g., data product files/catalogues need to be replaced with new versions), we will work towards an update
turnaround of ~1 week.
For non-critical changes (e.g., minor schema changes) we guarantee update timescales less than 1 month. To clarify whether a change is
critical or non-critical, `contact us <https://datacentral.org.au/contact/>`_.

Interface Issue
^^^^^^^^^^^^^^^
For critical interface issues (e.g., blocking data access) we aim to address any issue submitted by any user within 1 week
(though usually within 48 hours). For non-critical issues (e.g., mis-alignment on mobile view) we aim to publish a fix within 1 month.

Feature Requests
^^^^^^^^^^^^^^^^
Data Central is developed with its users in mind. We'd love to hear your suggestions for new features you'd
like to see developed, plus give you the opportunity to vote on the functionality you'd like to see prioritized.

Please suggest new features and vote on existing suggestions on our `requests page <https://datacentral.org.au/feature/>`_.
We use this information to guide our milestones and future development effort.


