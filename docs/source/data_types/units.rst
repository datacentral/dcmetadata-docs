.. _unit-best-practice:

Units within Data Central
-------------------------

Units and their representation is one of the more complex parts of the metadata
Data Central tracks, due to the existence of differing, contradictory standards
for representing units, and a large legacy of existing data which does not match
a particular standard chosen. Data Central uses standard tools (such as
astropy_) to read, convert, write and generally handle units in a VO-compliant
way as possible, and aim to fix all unit issues before any public data release.
In general, Data Central will not modify existing data releases to fix issues
with units.

In terms of actually representing units, we suggest reading VOUnits_, the IVOA
Recommendation on units within the Virtual Observatory, as it provides useful
information about how existing unit standards (or conventions), both within
astronomy and outside, specify how to encode units. The VOUnits document has
nothing particularly unusual, though it does note some corner cases worth taking
note of, such as treating the string ``'Pa'`` as Pascals rather than petayears
(which would happen if naively if ``'P'`` was always treated as representing
Peta).

In general, there is nothing particularly strange you need to do to produce data
with VO-compliant units, but there are two things you should probably check: the
capitalisation and pluralisation of the units; and encoding dimensionless or
unitless quantities.

Capitalisation and Pluralisation of Units
.........................................

Astropy by default reads only the singular form of a unit (e.g. "second" over
"seconds" or "Angstrom" over "Angstroms"), as generally this is the preferred
form in most unit standards. Data Central handles plural forms for existing data
releases, but for maximum compatibility, the singular form should be used for
new data (we are unaware of any software which rejects the singular form).

Similarly, lower case (except for units named after a person) is to be used, with
capitalisation of the first letter if the unit is named after a person (e.g.
"Angstrom" over "angstrom"). All caps (every letter being upper case) will throw
errors in astropy, and therefore should be avoided in new data.


Encoding Dimensionless or Unitless Quantities
.............................................

Neither the `FITS standard`_ nor the `IAU Style Guide`_ discuss the
representation of dimensionless/unitless quantities, so there is no specific
representation in standard FITS. There are some differing conventions mentioned
in the VOUnits document, with the VOUnits document recommending using an empty
string.

For normalised quantities, such as normalised intensity, we suggest using the
empty string for the unit, and describe the quantity in the comment (e.g. have
the FITS comment for the field be "Normalised Intensity" for something stored as
normalised intensity).


.. _VOUnits: http://www.ivoa.net/documents/VOUnits/
.. _astropy: https://astropy.org/
.. _`FITS standard`: https://fits.gsfc.nasa.gov/fits_standard.html
.. _`IAU Style Guide`: https://www.iau.org/publications/proceedings_rules/units/
