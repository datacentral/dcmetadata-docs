Metadata File Formats
=====================

Metadata files are always pipe-delimited, and have the extension .txt


Documentation
-------------
If you would like formatted text to appear in the `Schema Browser <https://datacentral.org.au/services/schema/>`_,
you can supply a "documentation" entry for groups and tables. The file itself can contain html text.

Please use the following extensions (to denote the format of a particular documentation file):

.html   contains html-formatted documentation

.txt     contains plain text-formatted documentation

If you do not wish to supply documentation for a particular row, leave that entry blank.

If you would like to include email addresses in your documentation, please format as:

.. code-block::

    <a class="is-mailable" data-person="john.smith" data-domain="sydney.edu.au"></a>

This is an anti-spam measure: rather than placing raw html (easily harvested by spambots),
a javascript-enabled browser is required to decode the address.

Documentation that you will want to update frequently should be maintained by the survey teams themselves in Data
Central's Documentation portal: Document Central. Contact us to get your team set up within the system if you have not
done so already.

.. important::

    Remember that the documentation used in the ingestion is the static,
    paper-like documentation, the documentation on Documentent Central is
    entirely separate.
