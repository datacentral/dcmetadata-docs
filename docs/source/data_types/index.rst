Data Types
==========
Currently Data Central supports catalogues, IFS, spectra and imaging data. View the sections below for details on file formatting and the required metadata for each data type.


.. warning::

    Please be sure to read the documentation carefully. Pay attention to
    restrictions on the contents of columns (e.g. restictions on allowed
    characters, choosing between a limited number of options), and double check
    that names match across different files. This reduces the number of issues
    encountered in ingestion, and reduces the number of iterations needed to
    finalise the data release.


.. toctree::
    :maxdepth: 2
    :caption: Data Types

    catalogues
    ifs
    images
    spectra
    data_file_formats
    meta_file_formats
    units
