Images
======

Ingesting fits images allows for their retrieval via the a multi-band image cutout service.
This service allows astronomers to easily and intuitively access imaging data from some of the world's
leading facilities alongside datasets of national significance, to quickly explore sources of interest
and produce publication quality images.

The image cutout service allows for comparison of the same source at different wavelengths side by side.
Users can input a single source/position or a list (automatically transforming between the submitted coordinate
frame and the native coordinate frame of the image) to generate single bands and/or three-colour images.

Five image scaling options are supported, along with custom plot options (axes style, colour map, coordinate grid),
over-plotting source positions from any of the DC-hosted input catalogues as well as remote VO datasets,
and over-plotting contours for a particular band at user-specified levels. Users are able to download results
as .png or .fits files, on a per-band/per-source basis, or download all the files from one request.

The cutout can host mosaiced image files in a range of projections and pixel scales, automatically reprojecting data on the fly.

.. note::

    Remember that the documentation mentioned here is the static, paper-like
    documentation, the documentation on Documentent Central is entirely
    separate.


Data Model
----------

Data Central's ingestion process will map your data onto the Data Central data model format.
Within Data Central, imaging files are organised hierarchically, as per:

::

    Survey
    └── DataRelease
        └── Schema:Imaging
            └── Facility
                └── Band
                    └── Region

Currently, the cutout service is not able to stitch together imaging on the fly.
You must provide FITS images that have been resampled and co-added (see for example, `SWarp <https://www.astromatic.net/software/swarp>`_.)
Each file is saved as a ``Region`` on the sky, with the fits header stored in the database to provide the coordinate
lookup. Note, this means individual region files must uniquely
cover one area of sky. Data central can not currently combine or display multiple
images in the same band covering the sam sky region. To explore the data model further, visit the imaging section of the
`Schema Browser <https://datacentral.org.au/services/schema/>`_ to explore the relationships between facilities and bands.



Directory Structure
-------------------
To ingest data into Data Central, you will provide two folders, one containing the imaging files themselves,
and one containing the metadata.

Data
^^^^
The imaging directory should contain the fits files themselves.

::

    data
    └── <survey>
        └── <datarelease>
            └── imaging
                ├── swpim_galex_FUV_d10.fits
                └── swpim_galex_NUV_g09.fits


.. Attention::
   ``<survey>`` and ``<datarelease>`` should be replaced with the values you chose in :doc:`Getting Started <../getting-started>`, e.g., gama and dr2

Data Central supports images in .fits formats. Note, these data files
can technically have any file name, as long as they are correctly
matching in your metadata files (see below). However, we strongly suggest using
sensible file names that describe the facility, band, survey and region.


Metadata
^^^^^^^^
The following file structure should be adopted.
A top-level ``<survey>`` directory should contain a single directory per ``<datarelease>``.
Both directories should have metadata files described below which will populate the `Schema Browser <https://datacentral.org.au/services/schema/>`_.


::

    metadata
    └── <survey>
        ├── <survey>_survey_meta.txt
        └── <datarelease>
            ├── <survey>_<data_release>_data_release_meta.txt
            └── imaging/
                ├── <survey>_<datarelease>_facility_meta.txt
                ├── <survey>_<datarelease>_band_meta.txt
                └── docs/

The metadata/imaging/ directory will contain 2 metadata files, plus a docs/ directory if
you have supplied additional documentation for a particular facility/band.


Metadata Files
--------------

.. Attention::
   Metadata files are always pipe-delimited, and have the extension .txt

<survey>_<datarelease>_facility_meta.txt
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This file describes the ``facilities`` you would like to register, and will be used to populate the Schema Browser.
Provide the following a single pipe-delimited .txt file containing an entry (row) for each facility:

+-----------------+-----------------+------------------------------------------------------------+--------------------------------------------+
| name            | pretty_name     | description                                                | documentation                              |
+=================+=================+============================================================+============================================+
| CFHTCFHTCOSMOS  | CFHT            | Images from the CFHT COSMOS survey.                        | unique_cfht_documentation_filename.txt     |
+-----------------+-----------------+------------------------------------------------------------+--------------------------------------------+

Please name this file: <survey>_<datarelease>_facility_meta.txt e.g., devils_dr1_facility_meta.txt



.. class:: <survey>_<datarelease>_facility_meta.txt

    This file should contain the following columns

    .. attribute:: name (required=True, type=char, max_limit=100)

        Facility name. Use only alphanumeric characters. This must be unique per data release.

    .. attribute:: pretty_name (required=True, type=char, max_limit=100)

        A human-readable version of the facility name. This can contain any characters (up to the character limit).

    .. attribute:: description (required=True, type=char, max_limit=1000)

        A succinct paragraph describing the facility.

    .. attribute:: documentation (required=True, type=char, max_limit=1000)

        If you would like formatted text to appear in the schema browser, please supply the name of the file containing
        html-formatted text (see :doc:`Formatting <./meta_file_formats>` for more info).
        Note, this is typically for 2-3 paragraphs of information. Detailed documentation should
        be written into a Document Central article. If you do not wish to supply documentation for a particular row,
        leave this entry blank.


<survey>_<datarelease>_band_meta.txt
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This file describes the ``bands`` you would like to register, and will be used to populate the Schema Browser and be
available to select in the cutout service.

Please provide a single .txt file with an entry per file (each file should have a corresponding unique 'region'), containing the following meta information:


+------+-------------+----------------+------------------+--------------+-------------+------------+----------+-----------+----------------+---------------+-----------------------+----------------------------+---------+-----------------------+--------+--------------+
| name | pretty_name | start_wave_ang | central_wave_ang | end_wave_ang | spatial_res | start_time | end_time | pol_state | facility       | description   | documentation         | contact                    | version | file_name             | region | is_displayed |
+======+=============+================+==================+==============+=============+============+==========+===========+================+===============+=======================+============================+=========+=======================+========+==============+
| H    | H           | 14629          | 16499.11         | 18085        | 0.5         |            |          |           | CFHTCFHTCOSMOS | COSMOS H-Band | unique_h-band_doc.txt | name <email@institute.org> | v02     | swpim_cfht_H_d10.fits | d10    | Y            |
+------+-------------+----------------+------------------+--------------+-------------+------------+----------+-----------+----------------+---------------+-----------------------+----------------------------+---------+-----------------------+--------+--------------+



Please name this file: <survey>_<datarelease>_band_meta.txt e.g., devils_dr1_band_meta.txt

.. class:: <survey>_<datarelease>_band_meta.txt

    This file should contain the following columns

    .. attribute:: name (required=True, type=char, max_limit=100)

        Band name. This must be unique per data release.

    .. attribute:: pretty_name (required=True, type=char, max_limit=100)

        A human-readable version of the band name. This can contain any characters (up to the character limit).

    .. attribute:: start_wave_ang (required=True, type=char, max_limit=100)

        A wavelength for the start of the filter bandpass of the image - in Angstrom. This is required by the Simple Image Access service.

    .. attribute:: central_wave_ang (required=True, type=char, max_limit=100)

        A rough central wavelength of the image - in Angstrom. This is used to order the bands in the cutout service options.

    .. attribute:: end_wave_ang (required=True, type=char, max_limit=100)

        A wavelength for the end of the filter bandpass of the image - in Angstrom. This is required by the Simple Image Access service.

    .. attribute:: spatial_res (required=True, type=char, max_limit=100)

        The spatial resolution (FWHM) of the image - in arcsec. This is required by the Simple Image Access service.

    .. attribute:: start_time (required=True, type=char, max_limit=100)

        The start time of the image observation in UTC isoformat: yyyy-mm-ddThh:mm:ss.ss. 
        In case of mosaics made of multiple images, this may either be left blank or preferably correspond to the start time of the earliest image in the mosaic. 
        This is required by the Simple Image Access service.

    .. attribute:: end_time (required=True, type=char, max_limit=100)

        The end time of the image observation in UTC isoformat: yyyy:mm:ddThh:mm:ss.ss. 
        In case of mosaics made of multiple images, this may either be left blank or preferably correspond to the end time of the latest image in the mosaic. 
        This is required by the Simple Image Access service.

    .. attribute:: pol_state (required=True, type=char, max_limit=100)

        The polarization state(s) of the image. An empty string if irrelevant (e.g. optical data), otherwise a space-separated string consisting of one or more of the following: I Q U V RR LL RL LR XX YY XY YX POLI POLA. This is required by the Simple Image Access service.

    .. attribute:: facility (required=True, type=char, max_limit=100)

        The name of the facility (must match a facility name from the `<survey>_<datarelease>_facility_meta.txt`_ file above)

    .. attribute:: description (required=True, type=char, max_limit=1000)

        A succinct paragraph describing the facility.

    .. attribute:: documentation (required=True, type=char, max_limit=1000)

        If you would like formatted text to appear in the schema browser, please supply the name of the file containing
        html-formatted text (see :doc:`Formatting <./meta_file_formats>` for more info).
        Note, this is typically for 2-3 paragraphs of information. Detailed documentation should
        be written into a Document Central article. If you do not wish to supply documentation for a particular row,
        leave this entry blank.

    .. attribute:: contact (required=True, type=char, max_limit=500)

        Format as: John Smith <john.smith@institute.org>

    .. attribute:: version (required=True, type=char, max_limit=100)

        Band version as defined by the team e.g., v1.8

    .. attribute:: file_name (required=True, type=char, max_limit=1000)

        The filename of the fits file you'll be providing. Note, you can provide multiple files at the same wavelength by
        providing an entry per file.

    .. attribute:: region (required=True, type=char, max_limit=1000)

        The name of the region for this file (e.g., g09, g12). You may choose to define this from the center RA of the image.

    .. attribute:: is_displayed (required=True, type=bool)

        Y or N. If N, this row will be skipped on ingestion (you may wish to give us the meta data now and the file later).

.. Attention::
   The fits header must contain the following keywords:

    +----------+------------------------------------+
    | Keyword  | Description                        |
    +==========+====================================+
    | GAIN     | Maximum equivalent gain (e-/ADU)   |
    +----------+------------------------------------+
    | CD1_1    | Linear projection matrix           |
    +----------+------------------------------------+
    | CD1_2    | Linear projection matrix           |
    +----------+------------------------------------+
    | CD2_1    | Linear projection matrix           |
    +----------+------------------------------------+
    | CD2_2    | Linear projection matrix           |
    +----------+------------------------------------+
    | NAXIS    | Number of axes                     |
    +----------+------------------------------------+
    | CRPIX1   | Reference pixel on this axis       |
    +----------+------------------------------------+
    | CRPIX2   | Reference pixel on this axis       |
    +----------+------------------------------------+
    | CRVAL1   | World coordinate on this axis      |
    +----------+------------------------------------+
    | CRVAL2   | World coordinate on this axis      |
    +----------+------------------------------------+
    | CTYPE1   | WCS projection type for this axis  |
    +----------+------------------------------------+
    | CTYPE2   | WCS projection type for this axis  |
    +----------+------------------------------------+
    | CUNIT1   | Axis unit                          |
    +----------+------------------------------------+
    | CUNIT2   | Axis unit                          |
    +----------+------------------------------------+
    | NAXIS1   | Number of elements along this axis |
    +----------+------------------------------------+
    | NAXIS2   | Number of elements along this axis |
    +----------+------------------------------------+
    | EQUINOX  | Mean equinox                       |
    +----------+------------------------------------+
    | RADECSYS | Astrometric system                 |
    +----------+------------------------------------+
