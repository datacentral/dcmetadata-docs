Policies
========

We reserve the right at any time to remove data that is in violation of any data usage licenses or agreements.

Data
----

When you release data through Data Central, all data and meta data are available for public download and distribution. Unless you request otherwise, data will be publicly released with a Creative Commons Attribution 4.0 International (also known as CC BY 4.0). See https://creativecommons.org/licenses/by/4.0/ for more details.


Who owns the data?
    You do! Any data that is uploaded to Data Central belongs to the researchers who created it. We are merely custodians of your data, allowing you to share it publicly or with your team.

What data can we share?
    We will only share public data and only with the owner's permission.

With whom can we share data?
    If data has been made public, then anyone has access to it. Private team data is only available to team members, and access is in the control of the team.



Maintenance
-----------

We aim to push new code to the production server on a regular basis and as such, guarantee a quick turnaround
for bug fixes and data updates. Downtime and maintenance windows are currently unscheduled, but we aim to keep disruption at a minimum (i.e., <1h).
You can check our twitter for maintenance notices `@DataCentralOrg <https://twitter.com/DataCentralOrg>`_.


