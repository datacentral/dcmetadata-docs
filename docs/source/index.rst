Data Central Ingestion Documentation
====================================

Data Central simplifies astronomical data releases by automating the ingestion, versioning, web-based data access,
and hosting of your data and supporting documentation.

Data Central has many features for hosting astronomical data including:

- Image cutout service produces publication-quality single-band or rgb images with custom image scaling options, fits creation, overplotting sources from DC-hosted and remote catalogues, batch mode, contours and more, over 30 bands across the GAMA and DEVILS regions (with VHS data coming soon)
- Single object viewer including image cutout at source position, data product download, 2D map visualizer, tabular data
- Cone search service including remote source position overlays
- Auto-generated, interactive, intuitive schema browser
- SQL/ADQL query service; return records that match some criteria from the catalogues hosted at Data Central
- TAP service
- Authenticated users can re-visit their query/cutout/cone search history


Your data will be discoverable alongside many of Australia's national optical surveys. Data Central also provides
access to our in-house CMS, Document Central, for your survey team to curate the data-release's supporting documentation.


.. tip::

    Data Central hosts a few different types of documentation:

        * static documentation on a specific data release (appearing in the
          schema browser)
        * updatable documentation about a survey (Document Central)
        * team wikis
        * documentation on how to use Data Central itself (including how to cite
          Data Central and the surveys we host).

    Only the first of these is discussed in the ingestion documentation, and
    only the first of these needs to be provided by the survey/data team before
    ingestion can begin. The others can be changed at any time, outside of the
    constraints of the ingestion process.

    Think of the first as a paper—you cannot change it once you've published it,
    you can only publish a new paper with the corrections
    (allowing citations)—whereas the others are like wikipedia, in that the
    content can be continuously updated, and hence is a poor source for
    citation.


.. toctree::
   :maxdepth: 2
   :caption: Contents

   getting-started
   data_types/index
   process
   policies
   license
   support
